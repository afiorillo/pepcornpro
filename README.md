# Pepcorn Pro

A website that helps you make popcorn.

Using all in-browser audio processing, this application tells you how many seconds since it last heard a "pop" from popcorn.
Conventional wisdom suggests that more than 5 seconds without a pop (while still heated) means nearly all the popcorn has popped.

## How to use

This is an all client web application, no backend, no tracking, no cookies, just popcorn.
Navigate to [the webpage](https://pepcorn.pro) on your phone or computer.

Prepare your popcorn cooker.
I like to [cook popcorn in a wok](https://whatthepopcorn.com/how-to-pop-popcorn-in-a-wok/), and preparing that usually takes a minute or two.
Once the popcorn is heating up, then you can click "Start" on the timer.

Adjust the slider at the bottom for the interval between pops which tells your the popcorn is done.
I prefer 5 seconds -- 5 seconds in between pops usually tells me there are only a handful of unpopped kernels left.
Once this interval is reached, celebrate and enjoy!

Click "Stop" to stop the timer.
Clicking "Start" will reset things.

## Developing

This is a Svelte application written in collaboration with ChatGPT (GPT-4).
While I drove the creative process and focused on architecture and design, ChatGPT capably did much of the grunt work.
At the same time, the code is pretty atrocious in my opinion.

To run locally:

```bash
$ npm install
$ npm run dev
```

## MIT License

Copyright (c) 2023 Andrew Fiorillo

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.