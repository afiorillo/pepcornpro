import adapter from '@sveltejs/adapter-static';

const dev = process.argv.includes('dev');


export default {
	kit: {
		adapter: adapter({
			// default options are shown. On some platforms
			// these options are set automatically — see below
			pages: 'build',
			assets: 'build',
			fallback: 'index.html',
			precompress: false,
			strict: true
		}),
		// no longer needed with the dedicated domain
		// uncomment and update if using Gitlab Pages
		// paths: {
		// 	base: dev ? '' : '/pepcornpro',
		// }
	}
};